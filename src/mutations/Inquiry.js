import gql from 'graphql-tag'

const InquiryMutation = gql`
  mutation CreateInquiryMutation($brandId: ID, $customerId: ID!) {
    createInquiry(brandId: $brandId, customerId: $customerId) {
      id
    }
  }
`

export default InquiryMutation
