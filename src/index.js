import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Switch } from 'react-router'
import { BrowserRouter } from 'react-router-dom';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo'

import {
  Home,
  Inquiry,
} from './components';
import './styles/index.scss';
import * as serviceWorker from './serviceWorker';

const client = new ApolloClient({
  uri: 'https://api.graph.cool/simple/v1/cjnngf3t50m670193c82v3q2f'
})

ReactDOM.render((
  <ApolloProvider client={client}>
    <BrowserRouter>
      <Switch>
        <Route exact path='/' component={Home} />
        <Route path='/inquiry/:inquiryId' component={Inquiry} />
      </Switch>
    </BrowserRouter>
  </ApolloProvider>
), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
