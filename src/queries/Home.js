import gql from 'graphql-tag'

const HomeQuery = gql`
  query HomeQuery {
    allCustomers(orderBy: lastName_DESC) {
      id
      name
      lastName
    }
    allBrands(orderBy: name_DESC) {
      id
      name
    }
    allInquiries(orderBy: createdAt_DESC) {
      id
      createdAt
      isFinished
      customer {
        id
        name
        lastName
      }
    }
  }
`

export default HomeQuery
