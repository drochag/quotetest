import gql from 'graphql-tag'

const InquiryQuery = gql`
  query InquiryQuery($id: ID) {
    Inquiry(id: $id) {
      id
      brand {
        id
        name
      }
      createdAt
      customer {
        id
        name
        lastName
      }
      customPaymentTerms
      deliveryDate
      destination
      freightTerms
      isCustomFactory
      isCustomPackaging
      isCustomProductTesting
      isCustomTrimLabel
      originCountry
      paymentTerms
      shipMode
      targetPrice
    }
  }
`

export default InquiryQuery
