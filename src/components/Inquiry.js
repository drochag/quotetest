import React from 'react'
import { Route, Redirect } from 'react-router'
import {
  Accessories,
  OrderInfo,
  Review,
  StyleInfo
} from '.';
import InquiryQuery from '../queries/Inquiry'

const Inquiry = ({ match, ...props }) => 
  <div className="Inquiry">
    <h1>Short Quote</h1>
    <Route exact path={`${match.path}/`} render={() => <Redirect to={`${match.url}styleInfo`} />} />
    <Route {...props} path={`${match.path}/styleInfo`} exact component={StyleInfo} />
    <Route {...props} path={`${match.path}/orderInfo`} exact component={OrderInfo} />
    <Route {...props} path={`${match.path}/accessories`} exact component={Accessories} />
    <Route {...props} path={`${match.path}/review`} exact component={Review} />
  </div>

export default Inquiry;
