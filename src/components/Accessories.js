import React, { Component } from 'react'
import { Query } from 'react-apollo'
import InquiryQuery from '../queries/Inquiry'
import { InquiryMainInfo, Steps } from '.'

class Accessories extends Component {
  constructor(props) {
    super(props)

    this.nextStep = this.nextStep.bind(this)
  }

  nextStep() {
    const { history, match } = this.props
    history.push(`/inquiry/${match.params.inquiryId}/review`)
  }

  render () {
    const { match } = this.props

    return <Query query={InquiryQuery} variables={{ id: match.params.inquiryId }} >
      {({ loading, error, data }) => {
        if (loading) {
          return <div>Loading Inquiry</div>
        }

        if (error) {
          return <div>Error: {error}</div>
        }

        const { customer, brand } = data.Inquiry
        return <div>
          <InquiryMainInfo customer={customer} brand={brand} />
          <Steps step={2} />
          <h1>Accessories</h1>
          <button className="Inquiry-button" onClick={this.nextStep}>Continue</button>
        </div>
      }}
    </Query>
  }
}

export default Accessories
