import React from 'react'
import classNames from 'classnames'

const steps = ['Style Info', 'Order Info', 'Accessories', 'Review']

const Steps = ({ step: currentStep }) => <div className="Steps">
  <ul className="Steps-dots">
    {steps.map((step, idx) =>
      <li
        key={step}
        className={classNames(
          'Steps-dotContainer',
          {
            'Steps-dotContainer--active': idx === currentStep,
            'Steps-dotContainer--first': idx === 0,
            'Steps-dotContainer--last': idx === steps.length - 1
          })}
      >
        <div className="Steps-dot" />
        <span className="Steps-label">{step}</span>
      </li>
    )}
  </ul>
</div>

export default Steps