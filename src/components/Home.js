import React, { Component } from 'react';
import { graphql } from 'react-apollo'
import classNames from 'classnames'

import HomeQuery from '../queries/Home'
import InquiryMutation from '../mutations/Inquiry'

class Home extends Component {
  constructor(props) {
    super(props)

    this.state = {
      customer: null,
      brand: null
    }

    this.selectCustomer = this.selectCustomer.bind(this)
    this.selectBrand = this.selectBrand.bind(this)
    this.createInquiry = this.createInquiry.bind(this)
  }

  selectInquiry(inquiry) {
    const { history } = this.props
    history.push(`/inquiry/${inquiry.id}/`)
  }

  async createInquiry() {
    const { customer, brand } = this.state
    const { history } = this.props
    const mutation = await this.props.createInquiryMutation({ variables: { customerId: customer.id, brandId: brand.id } })
    history.push(`/inquiry/${mutation.data.createInquiry.id}/`)
  }

  selectCustomer(customer) {
    this.setState({ customer })
  }

  selectBrand(brand) {
    this.setState({ brand })
  }

  render() {
    console.log(this.props)
    const { data } = this.props
    if (data.loading) {
      return <div className="Home">
        <div>Loading Customers, Brands and Quotes</div>
      </div>
    }

    return (
      <div className="Home">
        <h1 className="Home-title">Create new Inquiry</h1>
        <div className="Home-selectors">
          <div className="Home-selectorContainer">
            <h3 className="Home-selectorTitle">Select a Customer</h3>
            <ul className="Home-list">
              {data.allCustomers.map(customer =>
                <li
                  onClick={() => this.selectCustomer(customer)}
                  className={classNames('Home-listItem', { 'Home-listItem--selected': customer === this.state.customer })}
                  key={customer.id}
                >
                  {customer.name} {customer.lastName}
                </li>
              )}
            </ul>
          </div>
          <div className="Home-selectorContainer">
            <h3 className="Home-selectorTitle">Select a Brand</h3>
            <ul className="Home-list">
              {this.props.data.allBrands.map(brand =>
                <li
                  onClick={() => this.selectBrand(brand)}
                  className={classNames('Home-listItem', { 'Home-listItem--selected': brand === this.state.brand })}
                  key={brand.id}
                >
                  {brand.name}
                </li>
              )}
            </ul>
          </div>
          <div className="Home-selectorContainer">
            <h3 className="Home-selectorTitle">Go to Form</h3>
            <div className="Home-info">Select a Customer and a Brand to Continue</div>
            {(!this.state.brand || !this.state.customer) && <button className="Home-button" disabled>Create Inquiry</button>}
            {this.state.brand && this.state.customer && <button onClick={this.createInquiry} className="Home-button">Create Inquiry</button>}
          </div>
        </div>
        <h1 className="Home-title Home-title--inquiry">Inquiries</h1>
        <div className="Home-selectors">
          <div className="Home-selectorContainer Home-selectorContainer--full">
            <h3 className="Home-selectorTitle">Select an Inquiry to continue buying or see details</h3>
            <table className="Home-table">
              <thead>
                <tr>
                  <th>Inquiry #</th>
                  <th>Customer</th>
                  <th>Is finished ?</th>
                  <th>View / Continue</th>
                </tr>
              </thead>
              <tbody>
                {data.allInquiries.map((inquiry, idx) =>
                  <tr
                    className="Home-tableRow"
                    key={inquiry.id}
                  >
                    <td>{idx + 1}</td>
                    <td>{inquiry.customer.name} {inquiry.customer.lastName}</td>
                    <td>{inquiry.isFinished ? 'Yes' : 'No'}</td>
                    <td><button className="Home-button Home-button--small" onClick={() => this.selectInquiry(inquiry)}>{inquiry.isFinished ? 'View' : 'Continue'}</button></td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default graphql(InquiryMutation, { name: 'createInquiryMutation' })(graphql(HomeQuery)(Home));
