import React from 'react'

const InquiryMainInfo = ({ brand, customer }) => <div className="InquiryMainInfo">
  <img src="http://via.placeholder.com/200" className="InquiryMainInfo-img" />
  <div className="InquiryMainInfo-content">
    <h3>Brand: {brand.name}</h3>
    <h3>Customer: {customer.name} {customer.lastName}</h3>
  </div>
</div>

export default InquiryMainInfo