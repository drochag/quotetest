This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Folder Structure

```
quote/
  public/
    index.html
    favicon.ico
  server/
  src/
    assets/
    components/
    mutations/
    queries/
    styles/
```

* public

    All the static files used in the project, including the page template.

* server

    The Graphcool config, including all the types based on [this spreadsheet](https://docs.google.com/spreadsheets/d/1MHWDBIEFM8-_7eyXhiIigB4PVKAgcJGiWkvXZtD6-6U/edit#gid=810330866).

* src/assets

    All the assets that will be used in the project and compiled during build time. Not really used right now.

* src/components

    All possible reusable components.

    * Inquiry is the main component that will handle all the inquiry process and its routing.
    * Home is one additional page I created for the first part specs from the spreadsheet.

* src/mutations and src/queries

    Graphql queries to be used in the app

* styles

    All the styling for the app, I try to use BEM approach, but most likely following [this guidelines](https://github.com/gilbox/css-bliss).

## Process


### Learning 
I spent about 10 to 12 hours learning GraphQL and how Graphcool works, comparing Apollo and Relay (didn't go too deep because of time, I just checked a lot of examples on both and picked Apollo since it looked easier), revieweing different repos for GraphQL examples and analyzing the approach and usage of it since I've only used Relay once about 1 year ago and I was very used to Redux.

Also got distracted a lot of time also reading a bit of Prisma (my attention goes away often when learning new things, and for this everything was new).

### Setup 

I spent about 4 to 6 hours to set up the project. Since I haven't used GraphQL nor Apollo, in the way I found a couple of examples using Tachyos which I found nice for prototyping and decided to use it.

I also spent more time because all the examples found were using older versions of everything (react-router, react-apollo, etc).

Was able to make a query to graph.cool server successfully (Brands and Customers).

### Styling

As you can see on the styles all the styling was using extend, this was because after learning Tachyos I found out there were any mixins to use include instead :( . In an optimal case I would've used includes so I include like a config to my `index.scss` file including only the parts that were needed and reduce the stylesheet in a great way. I spent about 2 hours learning Tachyos.

Maybe this was not needed time since I decided by my own to learn up Tachyos.

### Autocomplete and first page

So the first section on the spreadsheet mentions a section that's not on the prototype so I decided to create a design (I'm not an excellent designer, but tried to have the same styles as the rest).

Later on, I tried different Autocomplete components with no success, I decided to drop that and put a simple list of brands and customers (homepage) to not waste more time. The issue here was my inexperience on using GraphQL approach in contrast with Redux.

I spent here about another 6 hours.

### More GraphQL setup

So after having the homepage functional I came in problems when trying to create an Inquiry, realizing graphql function is a HOC that I could use to wrap queries and mutations (duh). Spent about 2 hrs more on this.

### Quote process and real progress

I created all the routing, 4 steps of the quote, put them together and created the Main Info component and the Steps component. This was to show you how I work with styling and markup building. I spent about 5 hours on all the page excluding homepage and GraphQL setup. You can notice how I sped up in this part since I didn't need to design anything, opposed to home page which it was all my idea (and the autocomplete issues).

### All the time and summary

So far I spent:

* Learning - 12 hrs
* Setup - 4 to 6 hrs
* Styling - 2 hrs
* Autocomplete and homepage - 4hrs
* More GraphQL setup - 2hrs
* Real progress - 5hrs

Total: about 31hrs

### What I've learned

* GraphQL
    * queries
    * mutations
    * types, how to relate types between each other
* Graph.cool
    * How it works
    * How to deploy
    * How to use the playground
    * Also read a lot about the filesystem even though I couldn't get to use it due to time
* Tachyos
    * Learned everything about it and how to use it. Don't really recommend it with a higher css solutions (would be perfect for prototypes by its own, no need of scss)
* GraphQL approach

    I didn't completely understood all the approach, but really getting into it - for example I couldn't do the autocomplete input :(

